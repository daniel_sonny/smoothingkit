//
//  SmoothingOperator.swift
//  SmoothingKit
//
//  Created by Daniel Sonny Agliardi on 23/07/2017.
//  Copyright © 2017 DSA. All rights reserved.
//

import Foundation

//cut frequency = ~ 5.0 Hz

public class SmoothingOperator {
    
    private(set) var nl : Int = 0
    
    private(set) var nr : Int = 0
    
    private(set) var SGCoefficients : [Double] = []
    
    private var SGCoefficientsMatrix : Matrix<Double> {
        
        var SGCoefficientsMatrix = Matrix<Double>(rows: self.SGCoefficients.count,
                                                  columns: self.SGCoefficients.count,
                                                  repeatedValue: 0.0)
        
        for i in 0..<self.SGCoefficients.count {
            
            for j in 0..<self.SGCoefficients.count {
                
                SGCoefficientsMatrix[i, j] = self.SGCoefficients[j]
                
            }
            
        }
        
        return SGCoefficientsMatrix
        
    }
    
    
    //    private final List<Preprocessor> preprocessors = new ArrayList<Preprocessor>();
    
    /**
     * Constructs Savitzky-Golay filter which uses specified numebr of
     * surrounding data points
     *
     * @param nl
     *            numer of past data points filter will use
     * @param nr
     *            numer of future data points filter will use
     * @throws IllegalArgumentException
     *             of {@code nl < 0} or {@code nr < 0}
     */
    public init(nl : Int,
                nr : Int) {
        
        assert(!(nl < 0 || nr < 0), "Bad arguments")
        
        self.nl = nl
        
        self.nr = nr
        
        self.SGCoefficients = SmoothingOperator.computeSGCoefficients(nl: self.nl,
                                                                      nr: self.nr,
                                                                      degree: 4)
        
    }
    
    public func apply(onDoubleArray originalArray : [Double]) -> [Double] {
        
        let a = self.smooth(data: originalArray,
                            leftPad: [1.0, 1.0],
                            rightPad: [1.0, 1.0],
                            bias: 1,
                            coeffs: self.SGCoefficientsMatrix)
        
        return a
        
    }
    
    public func apply(onFloatArray originalArray : [Float]) -> [Float] {
        
        var b : [Double] = []
        
        for valueToAppend in originalArray {
            
            b.append(Double(valueToAppend))
            
        }
        
        let a = self.smooth(data: b,
                            leftPad: [1.0, 1.0],
                            rightPad: [1.0, 1.0],
                            bias: 1,
                            coeffs: self.SGCoefficientsMatrix)
        
        var smoothedArray : [Float] = []
        
        for value in a {
            
            smoothedArray.append(Float(value))
            
        }
        
        
        return smoothedArray
        
    }
    
    
    public static func test() {
        
        let filter = SmoothingOperator(nl: 3,
                                       nr: 3)
        
        let array = [5.0, 6.0]
        
        let smoothed = filter.apply(onDoubleArray: array)
        
        print(smoothed)
        
    }
    
    /**
     * Computes Savitzky-Golay coefficients for given parameters
     *
     * @param nl
     *            numer of past data points filter will use
     * @param nr
     *            number of future data points filter will use
     * @param degree
     *            order of smoothin polynomial
     * @return Savitzky-Golay coefficients
     * @throws IllegalArgumentException
     *             if {@code nl < 0} or {@code nr < 0} or {@code nl + nr <
     *             degree}
     */
    fileprivate static func computeSGCoefficients(nl : Int,
                                                  nr : Int,
                                                  degree : Int) -> [Double] {
        
        precondition(!(nl < 0 || nr < 0 || nl + nr < degree), "Bad arguments")
        
        var matrix : Matrix<Double> = Matrix(rows: degree + 1,
                                             columns: degree + 1, repeatedValue: 0)
        
        var sum : Double = 0
        
        for i in 0...degree {
            
            for j in 0...degree {
                
                sum = (i == 0 && j == 0) ? 1 : 0
                
                for k in 1...nr {
                    
                    sum += pow(Double(k), Double(i + j))
                    
                }
                
                for k in 1...nl {
                    
                    sum += pow(Double(-k), Double(i + j))
                    
                }
                
                matrix[i, j] = sum
                
            }
            
        }
        
        var b = [Double](repeating: 0.0, count: degree + 1)
        
        b[0] = 1
        
        b = Matrix.solve(matrix, b)
        
        var coeffs = [Double](repeating: 0.0, count: nl + nr + 1)
        
        for n in (-1*nl)...nr {
            
            sum = b[0];
            
            for m in 1...degree{
                
                sum += b[m] * pow(Double(n), Double(m))
                
            }
            
            coeffs[n + nl] = sum
            
        }
        
        return coeffs
        
    }
    
    /**
     * Smooths data by using Savitzky-Golay filter. Smoothing uses {@code
     * leftPad} and/or {@code rightPad} if you want to augment data on
     * boundaries to achieve smoother results for your purpose. If you do not
     * need this feature you may pass empty arrays (filter will use 0s in this
     * place, so you may want to use appropriate preprocessor). If you want to
     * use different (probably non-symmetrical) filter near both ends of
     * (padded) data, you will be using {@code bias} and {@code coeffs}. {@code
     * bias} essentially means
     * "how many points of pad should be left out when smoothing". Filters
     * taking this condition into consideration are passed in {@code coeffs}.
     * <tt>coeffs[0]</tt> is used for unbiased data (that is, for
     * <tt>data[bias]..data[data.length-bias-1]</tt>). Its length has to be
     * <tt>nr + nl + 1</tt>. Filters from range
     * <tt>coeffs[coeffs.length - 1]</tt> to
     * <tt>coeffs[coeffs.length - bias]</tt> are used for smoothing first
     * {@code bias} points (that is, from <tt>data[0]</tt> to
     * <tt>data[bias]</tt>) correspondingly. Filters from range
     * <tt>coeffs[1]</tt> to <tt>coeffs[bias]</tt> are used for smoothing last
     * {@code bias} points (that is, for
     * <tt>data[data.length-bias]..data[data.length-1]</tt>). For example, if
     * you use 5 past points and 5 future points for smoothing, but have only 3
     * meaningful padding points - you would use {@code bias} equal to 2 and
     * would pass in {@code coeffs} param filters taking 5-5 points (for regular
     * smoothing), 5-4, 5-3 (for rightmost range of data) and 3-5, 4-5 (for
     * leftmost range). If you do not wish to use pads completely for
     * symmetrical filter then you should pass <tt>bias = nl = nr</tt>
     *
     * @param data
     *            data for filter
     * @param leftPad
     *            left padding
     * @param rightPad
     *            right padding
     * @param bias
     *            how many points of pad should be left out when smoothing
     * @param coeffs
     *            array of filter coefficients
     * @return filtered data
     * @throws IllegalArgumentException
     *             when <tt>bias < 0</tt> or <tt>bias > min(nr, nl)</tt>
     * @throws IndexOutOfBoundsException
     *             when {@code coeffs} has less than <tt>2*bias + 1</tt>
     *             elements
     * @throws NullPointerException
     *             when any array passed as parameter is null
     */
    fileprivate func smooth(data : [Double],
                            leftPad : [Double],
                            rightPad : [Double],
                            bias : Int,
                            coeffs : Matrix<Double>) -> [Double] {
        
        precondition(!(bias < 0 || bias > self.nr || bias > self.nl), "bias < 0 or bias > nr or bias > nl")
        
        //    for (DataFilter dataFilter : dataFilters) {
        //
        //        data = dataFilter.filter(data);
        //
        //        }
        
        if data.isEmpty { return [] }
        
        let dataLength = data.count
        
        let n = dataLength + self.nl + self.nr
        
        var dataCopy = [Double](repeating: 0.0, count: n)
        
        // copy left pad reversed
        let leftPadOffset = nl - leftPad.count
        
        if leftPadOffset >= 0 {
            
            for i in 0..<leftPad.count {
                
                dataCopy[leftPadOffset + i] = leftPad[i]
                
            }
            
        }
        else {
            
            for i in 0..<nl {
                
                dataCopy[i] = leftPad[i - leftPadOffset];
                
            }
            
        }
        
        // copy actual data
        for i in 0..<dataLength {
            
            dataCopy[i + nl] = data[i]
            
        }
        
        
        // copy right pad
        let rightPadOffset = nr - rightPad.count
        
        if rightPadOffset >= 0 {
            
            for i in 0..<rightPad.count {
                
                dataCopy[i + dataLength + nl] = rightPad[i]
                
            }
        }
        else {
            
            for i in 0..<nr {
                
                dataCopy[i + dataLength + nl] = rightPad[i]
                
            }
            
        }
        
        //        for (Preprocessor p : preprocessors) {
        //
        //            p.apply(dataCopy);
        //
        //        }
        
        // convolution (with savitzky-golay coefficients)
        var sdata = [Double](repeating: 0.0, count: dataLength)
        
        var sg : [Double] = []
        
        for b in stride(from: bias, to: 1, by: -1) {
            
            sg = coeffs[row: coeffs.rows - b]
            
            let x = (self.nl + bias) - b;
            
            var sum = 0.0
            
            for i in -self.nl...self.nr {
                
                sum += dataCopy[x + i] * sg[self.nl - b + i]
                
            }
            
            sdata[x - nl] = sum
            
        }
        
        sg = coeffs[row: 0]
        
        for x in (self.nl + bias)..<(n - self.nr - bias) {
            
            var sum = 0.0
            
            
            for i in -self.nl...self.nr {
                
                sum += dataCopy[x + i] * sg[self.nl + i];
                
            }
            
            sdata[x - nl] = sum
            
        }
        
        for b in 1...bias {
            
            sg = coeffs[row: b]
            
            let x = (n - self.nr - bias) + (b - 1)
            
            var sum = 0.0
            
            for i in -self.nl...(self.nr - b) {
                
                sum += dataCopy[x + i] * sg[self.nl + i]
                
            }
            
            sdata[x - self.nl] = sum
            
        }
        
        return sdata
        
    }
    
}
