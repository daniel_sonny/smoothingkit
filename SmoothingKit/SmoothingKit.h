//
//  SmoothingKit.h
//  SmoothingKit
//
//  Created by Daniel Sonny Agliardi on 22/07/2017.
//  Copyright © 2017 DSA. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for SmoothingKit.
FOUNDATION_EXPORT double SmoothingKitVersionNumber;

//! Project version string for SmoothingKit.
FOUNDATION_EXPORT const unsigned char SmoothingKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SmoothingKit/PublicHeader.h>


