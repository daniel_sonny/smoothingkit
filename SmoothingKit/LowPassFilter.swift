//
//  LowPassFilter.swift
//  SmoothingKit
//
//  Created by Daniel Sonny Agliardi on 24/07/2017.
//  Copyright © 2017 DSA. All rights reserved.
//

import Foundation


public class LowPassFilter {
    
    fileprivate let GAIN_50HZ : Double = 2.072820954e+02
    
    fileprivate static let N_POLES : Int = 4
    
    fileprivate static let N_ZEROS : Int = 4
    
    fileprivate var xv = [Double](repeating: 0.0,
                              count: Int(LowPassFilter.N_ZEROS + 1))
    
    fileprivate var yv = [Double](repeating: 0.0,
                              count: Int(LowPassFilter.N_POLES + 1))
    
    public init() {}
    
    public func apply(onFloatArray signal : [Float]) -> [Float] {
        
        var a : [Double] = []
        
        for value in signal {
            
            a.append(Double(value))
            
        }
        
        let applied = self.apply(on: a)
        
        var b : [Float] = []
        
        for value in applied {
            
            b.append(Float(value))
            
        }
        
        return b
        
    }
    
    public func apply(on signal : [Double]) -> [Double] {
    
        let lastSignalSample = signal.last!
        
        self.xv.append(lastSignalSample / self.GAIN_50HZ)
        
        self.xv.remove(at: 0)
        
        self.yv.remove(at: 0)
        
        let a = self.xv[0]
        let b = self.xv[4]
        let d = 4 * (self.xv[1] + self.xv[3])
        let e = (6 * self.xv[2])
        let f = ( -0.1873794924 * self.yv[0])
        let g = (1.0546654059 * self.yv[1])
        let h = ( -2.3139884144 * self.yv[2])
        let i = (2.3695130072 * self.yv[3])
        
        let tmp = a + b + d + e + f + g + h + i
        
        self.yv.append(tmp)
        
        var result = Array(signal)
        
        result[result.count - 1] = self.yv[self.yv.count - 1]
        
        return result
        
    }
    
}
