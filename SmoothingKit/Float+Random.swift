//
//  Float+Random.swift
//  SmoothingKit
//
//  Created by Daniel Sonny Agliardi on 23/07/2017.
//  Copyright © 2017 DSA. All rights reserved.
//

import Foundation


public extension Float {
    
    public static var random:Float { get { return Float(arc4random()) / 0xFFFFFFFF } }
    
    public static func random(min: Float, max: Float) -> Float { return Float.random * (max - min) + min }
    
}
