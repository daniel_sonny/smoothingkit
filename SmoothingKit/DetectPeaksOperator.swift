//
//  DetectPeaksOperator.swift
//  SmoothingKit
//
//  Created by Daniel Sonny Agliardi on 24/07/2017.
//  Copyright © 2017 DSA. All rights reserved.
//

import Foundation


public class DetectPeaksOperator {
    
    public static let marker = -800.0
    
    public static func apply(onFloatArray signal: [Float],
                             andTimestamps timestamps : [Float]) -> (y: [Float], x: [Float]) {
        
        var b : [Double] = []
        
        for valueToAppend in signal {
            
            b.append(Double(valueToAppend))
            
        }
        
        var t : [Double] = []
        
        for valueToAppend in timestamps {
            
            t.append(Double(valueToAppend))
            
        }
        
        let a = self.apply(onDoubleArray: b,
                           andTimestamps: t)
        
        var bResult : [Float] = []
        
        for valueToAppend in a.y {
            
            bResult.append(Float(valueToAppend))
            
        }
        
        var tResult : [Float] = []
        
        for valueToAppend in a.x {
            
            tResult.append(Float(valueToAppend))
            
        }
        
        return (y: bResult, x: tResult)
        
    }
    
    public static func apply(onDoubleArray signal: [Double],
                             andTimestamps timestamps : [Double]) -> (y: [Double], x: [Double]) {
        
        precondition(signal.count == timestamps.count, "the given array MUST have the same size")
        
        
        if signal.isEmpty { return ([], []) }
        
        if signal.count == 1 { return (Array(signal),
                                      Array(timestamps)) }
        
        var result = (y: [Double](), x: [Double]())
        
        var y0 = signal.first!
        
        var timestamp0 = timestamps.first!
        
        var isRampingUp = false
        
        var isRampingDown = false
        
        result.x.append(timestamp0)
        
        
        for i in 1..<min(signal.count, timestamps.count) {
            
            //geting ready for current round
            let y1 = signal[i]
            
            let timestamp1 = timestamps[i]
            
            let oldIsRampingUp = isRampingUp
            
            
            //calculate current value
            isRampingUp = DetectPeaksOperator.isRampingUp(y0: y0,
                                                          y1: y1)
            
            isRampingDown = DetectPeaksOperator.isRampingDown(y0: y0,
                                                              y1: y1)
            
//            print("y0 : \(y0) - y1 : \(y1)")
//            print("x0 : \(timestamp0) - x1 : \(timestamp1)")
            
//            print("isRampingUp : \(isRampingUp) - isRampingDown : \(isRampingDown)")
            
            //calculating result for current round
            if oldIsRampingUp && isRampingDown { //cuspid found local max
                
                result.y.append(y0)
                
            } else {
            
                result.y.append(DetectPeaksOperator.marker)
            
            }
            
            result.x.append(timestamp1)
            
            
            //geting ready for next round
            y0 = y1
            
            timestamp0 = timestamp1
            
//            print("----------------")
            
        }
        
        //if a rampingUp it's found at the end 
        //then the last value is a local max too
        if DetectPeaksOperator.isRampingUp(y0: signal[signal.count - 2], y1: signal[signal.count - 1]){
            
            result.y.append(signal[signal.count - 1])
            
        } else {
            
            result.y.append(DetectPeaksOperator.marker)
            
        }
        
        //if a isRampingDown it's found at the start then
        //the first value of the signal is a local max too
        if DetectPeaksOperator.isRampingDown(y0: signal[0], y1: signal[1]) {
            
            result.y[0] = signal[0]
            
        } else {
            
            result.y[0] = DetectPeaksOperator.marker
            
        }
        
        
        return result
        
    }
    
    
    fileprivate static func isRampingUp (y0: Double,
                                         y1: Double) -> Bool { return y0 < y1 }
    
    
    fileprivate static func isRampingDown (y0: Double,
                                           y1: Double) -> Bool { return y0 > y1 }
    
    fileprivate static func isAPlateau (y0: Double,
                                        y1: Double) -> Bool { return y0 == y1 }
    
    public static func test() {
        
//        let signal : [Double] = [1, 2, 3, 2, 1, 0, 2, 0, 1, 4]
        
//        let signal : [Double] = [4, 1, 0, 2, 1, 0, 5, 3, 1, 5, 2]
        
//        let signal : [Double] = [4, 1, 0]
        
//        let signal : [Double] = [4, 1]
        
        let signal = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -8.55204391, 8.6475296, 5.26552582, -4.51579571, -7.85737324, 5.12745857, -2.22970486, 8.99452782, 3.3051548, 3.1677866, -3.16754627, 3.59005451, -5.32139635, 4.99568939, -2.82512522, 4.29729652, -8.38177967, -3.01918554, 1.22254944, -1.92019081, 2.84108353, -2.85564661, -1.89368343, 1.92489815, 3.69971466, -4.94572592, -1.0348525, -8.8839035, -8.18675709, 2.76006126, -0.801506042, 7.24636269, 7.30971718, 0.908643723, -5.15217781, 8.20359039, -4.23493576, -1.49795151, -2.82978058, 8.17928123, -0.506786346, -3.6586318, 4.19916344, -8.5851717]
        
        var floatSignal : [Float] = []
        
        for float in 0..<signal.count { floatSignal.append(Float(float)) }
        
        var timestamps : [Float] = []
        
        for t in 0..<signal.count { timestamps.append(Float(t)) }
        
        let a = DetectPeaksOperator.apply(onFloatArray: floatSignal,
                                          andTimestamps: timestamps)
        
        print(a)
        
    }
    
    
}
