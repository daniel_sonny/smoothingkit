//
//  Matrix+Operations.swift
//  SmoothingKit
//
//  Created by Daniel Sonny Agliardi on 24/07/2017.
//  Copyright © 2017 DSA. All rights reserved.
//

import Foundation

import Accelerate

extension Matrix {
    
    public static func inv(_ x : Matrix<Double>) -> Matrix<Double> {
        
        precondition(x.rows == x.columns, "Matrix must be square")
        
        var results = x
        
        var ipiv = [__CLPK_integer](repeating: 0, count: x.rows * x.rows)
        var lwork = __CLPK_integer(x.columns * x.columns)
        var work = [CDouble](repeating: 0.0, count: Int(lwork))
        var error: __CLPK_integer = 0
        var nc = __CLPK_integer(x.columns)
        
        dgetrf_(&nc, &nc, &(results.grid), &nc, &ipiv, &error)
        
        dgetri_(&nc, &(results.grid), &nc, &ipiv, &work, &lwork, &error)
        
        assert(error == 0, "Matrix not invertible")
        
        return results
        
    }
    
    public static func solve(_ A : Matrix<Double>,
                             _ B : [Double]) -> [Double] {
        
        //will compute X = inv(A) * B
        precondition(A.rows == A.columns, "Matrix must be square")
        
        precondition(A.rows == B.count, "Matrix and the array MUST have the same rows count")
        
        
        var X = [Double](repeating: 0.0, count: B.count)
        
        let invA = Matrix.inv(A)
        
        for i in 0..<invA.rows {
            
            var sum = 0.0
            
            for j in 0..<invA.columns {
                
                sum += invA[i, j] * B[j]
                
            }
            
            X[i] = sum
            
        }
        
        return X
        
        
    }
    
}
