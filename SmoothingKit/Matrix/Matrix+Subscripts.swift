//
//  Matrix+Subscripts.swift
//  SmoothingKit
//
//  Created by Daniel Sonny Agliardi on 24/07/2017.
//  Copyright © 2017 DSA. All rights reserved.
//

import Foundation

//MARK: Define access subscripts
extension Matrix {
    
    public subscript(row: Int, column: Int) -> Element {
        get {
            
            assert(indexIsValidForRow(row,
                                      column: column))
            
            return self.grid[(row * columns) + column]
            
        }
        
        set {
            
            assert(indexIsValidForRow(row,
                                      column: column))
            self.grid[(row * columns) + column] = newValue
            
        }
        
    }
    
    public subscript(row row: Int) -> [Element] {
        get {
            
            assert(row < rows)
            
            let startIndex = row * columns
            
            let endIndex = row * columns + columns
            
            return Array(grid[startIndex..<endIndex])
            
            
        }
        
        set {
            
            assert(row < rows)
            
            assert(newValue.count == columns)
            
            let startIndex = row * columns
            
            let endIndex = row * columns + columns
            
            grid.replaceSubrange(startIndex..<endIndex, with: newValue)
            
        }
        
    }
    
    fileprivate func indexIsValidForRow(_ row: Int,
                                        column: Int) -> Bool {
        
        return row >= 0 && row < rows && column >= 0 && column < columns
        
    }
    
}
