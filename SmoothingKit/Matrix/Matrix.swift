//
//  Matrix.swift
//  SmoothingKit
//
//  Created by Daniel Sonny Agliardi on 23/07/2017.
//  Copyright © 2017 DSA. All rights reserved.
//

import Foundation

public struct Matrix<T> where T: FloatingPoint, T: ExpressibleByFloatLiteral {
    
    public typealias Element = T
    
    let rows: Int
    let columns: Int
    var grid: [Element]
    
    public init(rows: Int, columns: Int, repeatedValue: Element) {
        
        self.rows = rows
        self.columns = columns
        
        self.grid = [Element](repeating: repeatedValue, count: rows * columns)
        
    }

    
    public static func test() {
    
        var matrix = Matrix<Double>(rows: 2,
                            columns: 2,
                            repeatedValue: 0.0)
        
        matrix[0, 0] = 2
        matrix[0, 1] = -5
        matrix[1, 0] = -2
        matrix[1, 1] = 4
        
        let B = [7.0, -6.0]
        
        let X = Matrix.solve(matrix, B)
        
        print(X)
        
    }
    
}
