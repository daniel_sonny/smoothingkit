# README #

### Purpose ###

This repo contains a set of mathematical operators

_This README is intended as full documentation that cover all assignment points._


### Explanation ###

SmoothingKit is an iOS/MacOs framework, that takes advantage of [Accelerate](https://developer.apple.com/documentation/accelerate), to provide some very basic mathematical operators useful to process a real time data put in place to try to archive all requirements about this topic.

The SmoothingKit provide the following utilities:

  * A struct for _Matrix_ objects.
  * [_Matrix Inversion_](https://en.wikipedia.org/wiki/Invertible_matrix) operator, used to solve _Linear systems_.
  * _Linear systems solver operator_, expressed in terms of matrix and array. It is used to calculate _Savitzky-Golay filter_ coefficients.
  * _Savitzky-Golay filter_ to smooth a signal. It is a common filter to smooth a signal by fitting successive sub-sets of adjacent data points with a low-degree polynomial.
  * _Butterworth filter_, a common and efficient low pass filter with cut frequency of 5Hz . From my past job experience, I learnt that a [IIR](https://en.wikipedia.org/wiki/Infinite_impulse_response) low pass filter can help the further analysis on an acceleration signal, in particular, if it is shown and processed real time (i. e. IIR filters have low delay because they are based on small buffers). This is my try to remove the higher frequency components, in order to keep only the useful part of the signal, in other words, this means improving the Signal to Noise Ratio.
  * _Peaks detect operator_ on a signal. This is implemented very easily by checking the sign of [slope](https://en.wikipedia.org/wiki/Slope) of the line between each couple of points from signal. When the slope became negative, so a local max value, or a peak, is detected.

### Compatibility ###

AmazingPlotApp is an iOS app, written in [Swift](https://en.wikipedia.org/wiki/Swift_(programming_language)) on last available Xcode version.
It is designed for iOS 10/MacOS 10.11, and above.


### Setup Instructions ###

The steps to add this framework:

  0. Clone locally the SmoothingKit repo;
  0. Add Xcode project file to an Stand-alone iOS/MacOS;
  0. On Xcode, from the project tree on the left, select the project itself;
  0. Select the target to which you want to add the SmoothingKit;
  0. Select "general" pane, if not already display, from top bar;
  0. Scroll down to "Embedded Binaries";
  0. Select SmoothingKit target for specific platform;
  0. Import SmoothingKit in any files where needed to use SmoothingKit defines.


### Repo Owner ###

Daniel Sonny Agliardi ([dany901@gmail.com](mailto:dany901@gmail.com), [Linkedin profile](https://www.linkedin.com/in/daniel-sonny-agliardi-136a9596/))
