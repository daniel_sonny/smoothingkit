//
//  SmoothingKitMacOS.h
//  SmoothingKitMacOS
//
//  Created by Daniel Sonny Agliardi on 23/07/2017.
//  Copyright © 2017 DSA. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for SmoothingKitMacOS.
FOUNDATION_EXPORT double SmoothingKitMacOSVersionNumber;

//! Project version string for SmoothingKitMacOS.
FOUNDATION_EXPORT const unsigned char SmoothingKitMacOSVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SmoothingKitMacOS/PublicHeader.h>


